FROM php:8-fpm

ARG APP_NAME=${APP_NAME}
ARG APP_ENV=${APP_ENV}
ARG APP_KEY=${APP_KEY}
ARG APP_DEBUG=${APP_DEBUG}
ARG APP_URL=${APP_URL}

ARG DB_CONNECTION=${DB_CONNECTION}
ARG DB_HOST=${DB_HOST}
ARG DB_PORT=${DB_PORT}
ARG DB_DATABASE=${DB_DATABASE}
ARG DB_USERNAME=${DB_USERNAME}
ARG DB_PASSWORD=${DB_PASSWORD}

ARG AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}
ARG AWS_URL=${AWS_URL}

RUN apt-get update && apt install -y zip unzip libzip4

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /app
COPY . .
# RUN composer update
# RUN php artisan config:cache
# RUN php artisan key:generate
RUN composer update
RUN composer upgrade
CMD php artisan serve --host=0.0.0.0 --port=8181
EXPOSE 8181